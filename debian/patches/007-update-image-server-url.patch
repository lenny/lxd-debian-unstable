From: Mathias Gibbens <gibmat@debian.org>
Description: Update various references of the Linux Containers image server to Canonical's image server. Based on upstream PRs 12748, 13208, and 13247.
Forwarded: not-needed
diff --git a/doc/cloud-init.md b/doc/cloud-init.md
index bbaead9c6..62429a130 100644
--- a/doc/cloud-init.md
+++ b/doc/cloud-init.md
@@ -30,11 +30,8 @@ Rebooting the instance does not re-trigger the actions.
 
 ## `cloud-init` support in images
 
-To use `cloud-init`, you must base your instance on an image that has `cloud-init` installed:
-
-* All images from the `ubuntu` and `ubuntu-daily` {ref}`image servers <remote-image-servers>` have `cloud-init` support.
-* Images from the [`images` remote](https://images.linuxcontainers.org/) have `cloud-init`-enabled variants, which are usually bigger in size than the default variant.
-  The cloud variants use the `/cloud` suffix, for example, `images:ubuntu/22.04/cloud`.
+To use `cloud-init`, you must base your instance on an image that has `cloud-init` installed, which is the case for all images from the `ubuntu` and `ubuntu-daily` {ref}`image servers <remote-image-servers>`.
+However, images for Ubuntu releases prior to `20.04` require special handling to integrate properly with `cloud-init`, so that `lxc exec` works correctly with virtual machines that use those images. Refer to {ref}`vm-cloud-init-config`.
 
 ## Configuration options
 
diff --git a/doc/external_resources.md b/doc/external_resources.md
index 344910ae8..38d54d961 100644
--- a/doc/external_resources.md
+++ b/doc/external_resources.md
@@ -4,5 +4,5 @@
 :maxdepth: 1
 
 Project repository <https://github.com/canonical/lxd>
-Community image server <https://images.linuxcontainers.org>
+Community image server <https://images.lxd.canonical.com>
 ```
diff --git a/doc/howto/benchmark_performance.md b/doc/howto/benchmark_performance.md
index e75bb9b81..2977d50d6 100644
--- a/doc/howto/benchmark_performance.md
+++ b/doc/howto/benchmark_performance.md
@@ -71,8 +71,8 @@ For example:
   - Description
 * - `lxd.benchmark init --count 10 --privileged`
   - Create ten privileged containers that use the latest Ubuntu image.
-* - `lxd.benchmark init --count 20 --parallel 4 images:alpine/edge`
-  - Create 20 containers that use the Alpine Edge image, using four parallel threads.
+* - `lxd.benchmark init --count 20 --parallel 4 ubuntu-minimal:22.04`
+  - Create 20 containers that use the Ubuntu Minimal 22.04 image, using four parallel threads.
 * - `lxd.benchmark init 2d21da400963`
   - Create one container that uses the local image with the fingerprint `2d21da400963`.
 * - `lxd.benchmark init --count 10 ubuntu`
diff --git a/doc/howto/images_remote.md b/doc/howto/images_remote.md
index 5f658cd3c..62c006f81 100644
--- a/doc/howto/images_remote.md
+++ b/doc/howto/images_remote.md
@@ -56,7 +56,7 @@ To reference an image, specify its remote and its alias or fingerprint, separate
 For example:
 
     ubuntu:22.04
-    images:ubuntu/22.04
+    ubuntu-minimal:22.04
     local:ed7509d7e83f
 
 (images-remote-default)=
diff --git a/doc/metadata.yaml b/doc/metadata.yaml
index e34d5e38d..630c3eae8 100644
--- a/doc/metadata.yaml
+++ b/doc/metadata.yaml
@@ -139,5 +139,5 @@ navigation:
           external: true
 
         - title: Image server
-          location: https://images.linuxcontainers.org
+          location: https://images.lxd.canonical.com
           external: true
diff --git a/doc/reference/remote_image_servers.md b/doc/reference/remote_image_servers.md
index 622a1ea27..524994471 100644
--- a/doc/reference/remote_image_servers.md
+++ b/doc/reference/remote_image_servers.md
@@ -20,12 +20,6 @@ The `lxc` CLI command comes pre-configured with the following default remote ima
 
   See [`cloud-images.ubuntu.com/daily`](https://cloud-images.ubuntu.com/daily/) for an overview of available images.
 
-`images:`
-: This server provides unofficial images for a variety of Linux distributions.
-  The images are maintained by the [Linux Containers](https://linuxcontainers.org/) team and are built to be compact and minimal.
-
-  See [`images.linuxcontainers.org`](https://images.linuxcontainers.org) for an overview of available images.
-
 (remote-image-server-types)=
 ## Remote server types
 
diff --git a/doc/rest-api.yaml b/doc/rest-api.yaml
index c35879ffd..52cd8d9cd 100644
--- a/doc/rest-api.yaml
+++ b/doc/rest-api.yaml
@@ -973,7 +973,7 @@ definitions:
                 x-go-name: Protocol
             server:
                 description: URL of the source server
-                example: https://images.linuxcontainers.org
+                example: https://cloud-images.ubuntu.com/releases
                 type: string
                 x-go-name: Server
         type: object
@@ -1088,7 +1088,7 @@ definitions:
                 x-go-name: Secret
             server:
                 description: URL of the source server
-                example: https://images.linuxcontainers.org
+                example: https://cloud-images.ubuntu.com/releases
                 type: string
                 x-go-name: Server
             type:
@@ -2043,7 +2043,7 @@ definitions:
                 x-go-name: Websockets
             server:
                 description: Remote server URL (for remote images)
-                example: https://images.linuxcontainers.org
+                example: https://cloud-images.ubuntu.com/releases
                 type: string
                 x-go-name: Server
             source:
diff --git a/lxc/config/default.go b/lxc/config/default.go
index caa4bdf53..89475f3b0 100644
--- a/lxc/config/default.go
+++ b/lxc/config/default.go
@@ -7,9 +7,9 @@ var LocalRemote = Remote{
 	Public: false,
 }
 
-// ImagesRemote is the community image server (over simplestreams).
+// ImagesRemote is the main image server (over simplestreams).
 var ImagesRemote = Remote{
-	Addr:     "https://images.linuxcontainers.org",
+	Addr:     "https://images.lxd.canonical.com",
 	Public:   true,
 	Protocol: "simplestreams",
 }
@@ -49,6 +49,7 @@ var UbuntuMinimalDailyRemote = Remote{
 // StaticRemotes is the list of remotes which can't be removed.
 var StaticRemotes = map[string]Remote{
 	"local":                LocalRemote,
+	"images":               ImagesRemote,
 	"ubuntu":               UbuntuRemote,
 	"ubuntu-daily":         UbuntuDailyRemote,
 	"ubuntu-minimal":       UbuntuMinimalRemote,
@@ -57,7 +58,6 @@ var StaticRemotes = map[string]Remote{
 
 // DefaultRemotes is the list of default remotes.
 var DefaultRemotes = map[string]Remote{
-	"images":               ImagesRemote,
 	"local":                LocalRemote,
 	"ubuntu":               UbuntuRemote,
 	"ubuntu-daily":         UbuntuDailyRemote,
diff --git a/lxc/config/file.go b/lxc/config/file.go
index 9b68eba82..c845b7100 100644
--- a/lxc/config/file.go
+++ b/lxc/config/file.go
@@ -74,17 +74,6 @@ func LoadConfig(path string) (*Config, error) {
 		c.DefaultRemote = DefaultConfig().DefaultRemote
 	}
 
-	// NOTE: Remove this once we only see a small fraction of non-simplestreams users
-	// Upgrade users to the "simplestreams" protocol
-	images, ok := c.Remotes["images"]
-	if ok && images.Protocol != ImagesRemote.Protocol && images.Addr == ImagesRemote.Addr {
-		c.Remotes["images"] = ImagesRemote
-		err = c.SaveConfig(path)
-		if err != nil {
-			return nil, err
-		}
-	}
-
 	return c, nil
 }
 
diff --git a/lxd-benchmark/main.go b/lxd-benchmark/main.go
index be26e2a36..87842490e 100644
--- a/lxd-benchmark/main.go
+++ b/lxd-benchmark/main.go
@@ -96,8 +96,8 @@ func main() {
 	app.Example = `  # Spawn 20 Ubuntu containers in batches of 4
   lxd-benchmark launch --count 20 --parallel 4
 
-  # Create 50 Alpine containers in batches of 10
-  lxd-benchmark init --count 50 --parallel 10 images:alpine/edge
+  # Create 50 Ubuntu Minimal 22.04 containers in batches of 10
+  lxd-benchmark init --count 50 --parallel 10 ubuntu-minimal:22.04
 
   # Delete all test containers using dynamic batch size
   lxd-benchmark delete`
diff --git a/lxd/instance_instance_types.go b/lxd/instance_instance_types.go
index 8cdb5ff5e..662d3ab18 100644
--- a/lxd/instance_instance_types.go
+++ b/lxd/instance_instance_types.go
@@ -103,7 +103,7 @@ func instanceRefreshTypesTask(d *Daemon) (task.Func, task.Schedule) {
 func instanceRefreshTypes(ctx context.Context, s *state.State) error {
 	// Attempt to download the new definitions
 	downloadParse := func(filename string, target any) error {
-		url := fmt.Sprintf("https://images.linuxcontainers.org/meta/instance-types/%s", filename)
+		url := fmt.Sprintf("https://images.lxd.canonical.com/meta/instance-types/%s", filename)
 
 		httpClient, err := util.HTTPClient("", s.Proxy)
 		if err != nil {
diff --git a/shared/api/image.go b/shared/api/image.go
index d675378ad..9c90fea2e 100644
--- a/shared/api/image.go
+++ b/shared/api/image.go
@@ -221,7 +221,7 @@ type ImageSource struct {
 	Protocol string `json:"protocol" yaml:"protocol"`
 
 	// URL of the source server
-	// Example: https://images.linuxcontainers.org
+	// Example: https://cloud-images.ubuntu.com/releases
 	Server string `json:"server" yaml:"server"`
 
 	// Type of image (container or virtual-machine)
diff --git a/shared/api/instance.go b/shared/api/instance.go
index 40c7d29c6..cd5e4d406 100644
--- a/shared/api/instance.go
+++ b/shared/api/instance.go
@@ -299,7 +299,7 @@ type InstanceSource struct {
 	Properties map[string]string `json:"properties,omitempty" yaml:"properties,omitempty"`
 
 	// Remote server URL (for remote images)
-	// Example: https://images.linuxcontainers.org
+	// Example: https://cloud-images.ubuntu.com/releases
 	Server string `json:"server,omitempty" yaml:"server,omitempty"`
 
 	// Remote server secret (for remote private images)
diff --git a/shared/simplestreams/products.go b/shared/simplestreams/products.go
index a07e4d5b1..d4f3ce93a 100644
--- a/shared/simplestreams/products.go
+++ b/shared/simplestreams/products.go
@@ -10,8 +10,8 @@ import (
 	"github.com/canonical/lxd/shared/osarch"
 )
 
-var lxdCompatCombinedItems = []string{"lxd_combined.tar.gz", "incus_combined.tar.gz"}
-var lxdCompatItems = []string{"lxd.tar.xz", "incus.tar.xz"}
+var lxdCompatCombinedItems = []string{"lxd_combined.tar.gz"}
+var lxdCompatItems = []string{"lxd.tar.xz"}
 
 // Products represents the base of download.json.
 type Products struct {
diff --git a/shared/util_test.go b/shared/util_test.go
index 2c9b177c7..70fd61bbd 100644
--- a/shared/util_test.go
+++ b/shared/util_test.go
@@ -25,16 +25,16 @@ func TestURLEncode(t *testing.T) {
 }
 
 func TestUrlsJoin(t *testing.T) {
-	baseUrl := "http://images.linuxcontainers.org/streams/v1/"
+	baseURL := "https://cloud-images.ubuntu.com/releases/streams/v1/"
 	path := "../../image/root.tar.xz"
 
-	res, err := JoinUrls(baseUrl, path)
+	res, err := JoinUrls(baseURL, path)
 	if err != nil {
 		t.Error(err)
 		return
 	}
 
-	expected := "http://images.linuxcontainers.org/image/root.tar.xz"
+	expected := "https://cloud-images.ubuntu.com/releases/image/root.tar.xz"
 	if res != expected {
 		t.Error(fmt.Errorf("'%s' != '%s'", res, expected))
 	}
